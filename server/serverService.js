import { ApolloServer } from 'apollo-server-express';
import Server from 'express';
import bodyParser from 'body-parser';
import * as URI from 'uri-js';
import appModule from './modules';
import AssertionTokenType from './token-types/AssertionTokenType';
import TokenType from './token-types/TokenType';

import db from './db';
import server_config from './config/server_config';

const {
  dispatch, spawn, stop, query,
} = require('nact');

const jose = require('node-jose');

const commandLineArgs = require('command-line-args');

const argsDefinitions = [
  { name: 'ci', type: Boolean },
];
const args = commandLineArgs(argsDefinitions);

const reset = async (msg, error, ctx) => {
  if (error) {
    console.log(error);
  }
  // return ctx.reset;
};

export default (parent) => spawn(
  parent,
  async (state, msg, ctx) => {
    let serverConfigActor;
    if (ctx.children.has('server_config')) {
      serverConfigActor = ctx.children.get('server_config');
    }

    const config = await query(serverConfigActor, {}, 20000);
    const { host, port } = URI.parse(config.uri);
    console.log(`Welcome to Ecosystem (${config.application_type})`);

    try {
      const { schema, context } = appModule(ctx);
      const apolloServer = new ApolloServer({
        schema,
        context,
        debug: server_config.debug ? server_config.debug : false,
        formatError: (err) => {
          console.log(err);
          return err;
        },

        introspection: true,
      });

      // const apolloServer = new ApolloServer(schema(ctx));

      //                host: state.uri,
      //                 port: state.port,

      const app = new Server();
      app.listen({
        host,
        port,
        routes: {
          cors: {
            origin: ['*'],
            headers: ['Accept', 'Content-Type', 'id_token', 'authorization'],
            additionalHeaders: ['X-Requested-With'],
          },
        },
      }, () => {
      });
      app.use('/static', Server.static('./static'));
      // await
      await apolloServer.applyMiddleware({
        app,
      });

      if (args.ci) {
        process.exit();
      }

      // await
      // await apolloServer.installSubscriptionHandlers(app.listener);

      // await
      // await app.start();
    } catch (e) {
      console.log(e);
    }

    // config
  },
  'server',
  { onCrash: reset },
);

// throw new AuthorizationError('you must be logged in to query this schema');

// https://www.npmjs.com/package/role-acl
// https://onury.io/accesscontrol/?content=faq
// https://developer.mindsphere.io/concepts/concept-roles-scopes.html
// https://wso2.com/library/articles/2015/12/article-role-based-access-control-for-apis-exposed-via-wso2-api-manager-using-oauth-2.0-scopes/
// https://onury.io/accesscontrol/
